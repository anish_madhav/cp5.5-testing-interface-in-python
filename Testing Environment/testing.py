#!/usr/bin/env python3
import sys, contextlib, io, os, json, datetime, pandas, argparse, math, time, logging
from multiprocessing.pool import ThreadPool
import subprocess
sys.path.append(sys.path[0]+'/..')
import viewos_python_framework
import view_mqtt_comms
sys.path.append(sys.path[0]+'/../device/common')
from scanDeviceInfo import main as scanDeviceInfo
sys.path.append(sys.path[0]+'/../device/cphe')
from setTrunkOnOff import main as setTrunkOnOff
from getTrunkData import main as getTrunkData

LOG_FOLDER = "cp_logs"

log_file_session_name = ""

def set_up_logger(_level, _log_to_tmp, _serial_number):
    global log_file_session_name

    if _log_to_tmp is False:
        LOG_FOLDER_PATH = os.path.join(os.path.dirname((os.path.realpath(__file__))), LOG_FOLDER)
    else:
        LOG_FOLDER_PATH = os.path.join('/tmp', LOG_FOLDER)

    try:
        os.mkdir(LOG_FOLDER_PATH)
    except:
        pass

    # Set up logger
    logger = logging.getLogger(__name__)

    # Set up logger to stdout
    handler = logging.StreamHandler(stream=sys.stdout)
    formatter = logging.Formatter('[%(levelname)s] %(message)s')
    handler.setFormatter(formatter)
    handler.setLevel(logging.DEBUG)
    logger.addHandler(handler)

    # Set up logger to the log file
    date_time = datetime.datetime.today().strftime('%Y-%m-%d_%H-%M-%S')
    log_file_session_name = os.path.basename((os.path.realpath(__file__)))[:-3] + '_SN' + _serial_number + '_' + date_time
    log_file_name =  log_file_session_name + '.txt'
    log_file_path = os.path.join(LOG_FOLDER_PATH, log_file_name)

    handler = logging.FileHandler(log_file_path)
    formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s', datefmt='%H:%M:%S')
    handler.setFormatter(formatter)
    handler.setLevel(logging.DEBUG)
    logger.addHandler(handler)

    logger.setLevel(_level)
    logger.info("The log file has been created at " + log_file_path)
    return logger, log_file_path

def main(serial_number, log_to_tmp_flag):

    command = "sudo :"
    process = subprocess.Popen([command],
                    stdout=subprocess.PIPE, 
                    stderr=subprocess.PIPE,
                    shell=True,
                    executable='/bin/bash')

    logger, log_file_path = set_up_logger(logging.INFO, log_to_tmp_flag, serial_number)

    with contextlib.redirect_stdout(io.StringIO()):         # stdout suppressed
        device_df = scanDeviceInfo(True)

    with contextlib.redirect_stdout(io.StringIO()):         # stdout suppressed
        view_mqtt_comms.start(False)

    #################################################################################
    logger.info("Step 1. Validate 2 CPHEL's are seen on the application layer ")
    cphe_df = device_df.loc[(device_df["manu"] == "VIEW") & 
                            (device_df["modl"] == "CPHE") & 
                            (device_df["revi"] == "Gn10")]
    logger.info(list(cphe_df.index))
    if len(cphe_df) != 2:
        logger.error("There should be 2 CPHE's online but we found " + str(len(cphe_df)) + ". Stop the script")
        sys.exit(-1)

    #################################################################################
    logger.info("Step 2. Validate 1 FANC is seen on the application layer")
    fanc_df = device_df.loc[(device_df["manu"] == "VIEW") & 
                            (device_df["modl"] == "FANC") & 
                            (device_df["revi"] == "Gn10")]
    logger.info(list(fanc_df.index))
    if len(fanc_df) != 1:
        logger.error("There should be 1 FANC online but we found " + str(len(fanc_df)) + ". Stop the script")
        sys.exit(-1)

    #################################################################################
    logger.info("Step 3. Validate that all 24/32 NWCs are connecting to the broker ")
    ntwc_df = device_df.loc[(device_df["manu"] == "VIEW") & 
                            (device_df["modl"] == "NtWC") & 
                            (device_df["revi"] == "Pr10")]
    logger.info(list(ntwc_df.index))
    if len(ntwc_df) != 24 and len(ntwc_df) != 32 :
        logger.error("There should be 24 or 32 NtWC online but we found " + str(len(ntwc_df)) + ". Stop the script")
        sys.exit(-1)

    #################################################################################
    logger.info("Step 4. Turn off all trunks")

    with contextlib.redirect_stdout(io.StringIO()):         # stdout suppressed
        for cphe_device_id in list(cphe_df.index):
            logger.info("Turning off " + str(cphe_device_id) + "\'s trunks")
            for i in range(0,16):
                setTrunkOnOff(view_mqtt_comms, int(cphe_device_id, 0), i, "off")

    #################################################################################
    logger.info("Step 5. Turn all trunks back on and validate that all NWCs are seen (1 on each trunk)")

    def count_number_of_dm_and_ep():
        command = "pushd ../../network; sudo python3 ./test_ghn_lib.py --csv; popd"
        process = subprocess.Popen([command],
                        stdout=subprocess.PIPE, 
                        stderr=subprocess.PIPE,
                        shell=True,
                        executable='/bin/bash')
        stdout, stderr = process.communicate()
        if stderr != b'':
            print(stderr)
            sys.exit(-1)
        stdout_string = stdout.decode("utf-8")

        csv_output = ""
        for line in stdout_string.split("\n"):
            if ',' in line:
                csv_output = csv_output + line + "\n"

        with io.StringIO() as f:
            f.write(csv_output)
            f.seek(0)
            csv_df = pandas.read_csv(f)
            dm_df = csv_df[csv_df["type"]=="DM"]
            ep_df = csv_df[csv_df["type"]=="EP"]
            return len(ep_df)

    previous_num_of_ep = 0
    with contextlib.redirect_stdout(io.StringIO()):         # stdout suppressed
        for cphe_device_id in list(cphe_df.index):
            logger.info("Turning on " + str(cphe_device_id) + "\'s trunks")
            for i in range(0,16):
                logger.info("Trunk #" + str(i) + "\'s on " + str(cphe_device_id))
                setTrunkOnOff(view_mqtt_comms, int(cphe_device_id, 0), i, "on")
                time.sleep(20)
                current_number_of_ep = count_number_of_dm_and_ep()
              #  if current_number_of_ep != previous_num_of_ep + 1:
              #      logger.error("There should exactly 1 new NtWC online but we found " + str(current_number_of_ep - previous_num_of_ep) + ". Stop the script")
              #      sys.exit(-1)
              #  previous_num_of_ep = current_number_of_ep
                logger.info("Current count EP #" + str(current_number_of_ep))

    #################################################################################
    # Loop so all NWCs can connect
    logger.info("Waiting for all the NWCs to connect")
    for x in range(85):
        current_number_of_ep = count_number_of_dm_and_ep()
        if current_number_of_ep == 24: break
        elif current_number_of_ep == 32: break
        else:
           print("Just Wait")
           continue
#        print(current_number_of_ep)

    logger.info("Current count EP #" + str(current_number_of_ep))

     #################################################################################
    logger.info("Validate that all 24/32 NWCs are connecting to the broker ")
    ntwc_df = device_df.loc[(device_df["manu"] == "VIEW") &
                            (device_df["modl"] == "NtWC") &
                            (device_df["revi"] == "Pr10")]
    logger.info(list(ntwc_df.index))
    if len(ntwc_df) != 24 and len(ntwc_df) != 32 :
        logger.error("There should be 24 or 32 NtWC online but we found " + str(len(ntwc_df)) + ". Stop the script")
        sys.exit(-1)

    #################################################################################

    logger.info("Step 6. Record the current for each trunk on each CPHEL")
    logger.info("Step 7. Record the voltage for each trunk on each CPHEL")

    with contextlib.redirect_stdout(io.StringIO()):         # stdout suppressed
        for cphe_device_id in list(cphe_df.index):
            logger.info("Get current/voltage values of " + str(cphe_device_id) + "\'s trunks")
            cphe_iv_values = getTrunkData(view_mqtt_comms, int(cphe_device_id, 0), True, False)
            logger.info("\n"+cphe_iv_values)

    view_mqtt_comms.stop()
    #################################################################################
    logger.info("Step 8. Record the temperature readings from FANC")

    fanc_device_id_list = list(fanc_df.index)

    command = "pushd ../mqtt; sudo python3 mqttViewer.py -t 70 -d " + fanc_device_id_list[0] + "; popd"
    process = subprocess.Popen([command],
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                    shell=True,
                    executable='/bin/bash')
    stdout, stderr = process.communicate()
    if stderr != b'':
        print(stderr)
        sys.exit(-1)
    stdout_string = stdout.decode("utf-8")
    found_temperature_flag = False
    for line in stdout_string.split("\n"):
        if "state" in line:
            logger.info(line)
        if "fanRpm" in line:
            logger.info(line)
        if "tempDegC" in line:
            logger.info(line)
            found_temperature_flag = True

    if found_temperature_flag is False:
        logger.error("Could not fetch the temperature info from " + fanc_device_id_list[0] + ". Stop the script")
        sys.exit(-1)

    #################################################################################
    # logger.info("Step 9. Provide a PASS or FAIL.  Report failing issues")
    # logger.info("Note: The script would not reach here if failed")
    #################################################################################
    # logger.info("Step 10. Log all data for the SN")
    logger.info("End of the script")
    logger.info("PASS")
    print("Logs are stored at " + log_file_path)

    print("\nPASS\n")

if __name__ == "__main__":

    print("\n\n********************************************************")
    print("*                                                      *")
    print("*   This script validate the CP5 control panel.        *")
    print("*                                                      *")
    print("********************************************************\n")

    parser = argparse.ArgumentParser(description='Validate the CP5 control panel.')
    parser.add_argument('serial_number', type=str, help='The serial number of the controler panel')
    parser.add_argument('-t', '--log_to_tmp', action="store_true", help='Log in /tmp folder')

    args = parser.parse_args()

    main(args.serial_number, args.log_to_tmp)
