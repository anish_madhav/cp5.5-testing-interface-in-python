# CP5.5 Testing Interface in Python


This README file contains intstructions on how to access and run the Python code for the CP5.5 testing web Interface.
It contains 5 different files:
- auth.py which is used for user authentication before the testing can begin
- init.py which houses all the main functions for different tests
- models.py which has the functions for all the testing models and how to run
- testing.py which is the code for the testing web Interface
- views.py which is linked to the HTML aspect of the Interface

The above Python files are run in a virtual environment which makes if safer and easier to access.
All the above Python files include the usage of Flask, JavaScript and HTML

The main function houses extra functionalities in case any new tests or observations need to added in the future. Also, make sure to update the main function accordingly with the testing function as the Virtual environment does not auto-commit.

----------------------------------xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx-----------------------------
